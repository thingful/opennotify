# Example Thingful fetcher for http://open-notify.org/

## Description

Open Notify (http://open-notify.org/) provides a simple API which returns the
current location (lat/long) of the International Space Station using data
provided by NASA.

This project contains a library capable of retrieving that data and returning
it in a form ready for storing on Thingful.

## Authentication

Not required
